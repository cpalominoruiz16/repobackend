const express = require('express'); //referencia al paquete express
const userFile = require('./user.json');
const bodyParser= require('body-parser');
var app= express(); //paquete express nos va permitir hacer peticiones http
app.use(bodyParser.json()) //para este servidor node app habilita el parseador bodyParser
var port=process.env.PORT || 3000;
const URL_BASE = '/api-peru/v1/';

//operaciòn GET del 'Hola mundo'ademas todos los usuarios (users.json)
/*app.get ('/holamundo',
    function(request,response){
        response.send('HOLA GENTita soy charles--->');
    });
*/

//obtener la cantidad usuario y mandar como json en el send

app.get (URL_BASE+'users2',
    function(request,response){
        let cantidad=userFile.length
        console.log("Cantidad de usuario: "+cantidad);
        //var numElementosJson = []
        //numElementosJson.push(cantidad)
        let totalJson = JSON.stringify({num_elem: cantidad});
        response.status(200).send(totalJson);

    });

//operation get a un usuario con id (instance) 'users/:id/:id2/:id3'
app.get (URL_BASE+'users/:id/:id2',
    function(request,response){
//variable local let
      let indice = request.params.id;
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      response.status(200);
      response.send(userFile[indice-1]);
      response.send(userFile[indice2-2]);
    });


//operation get a un usuario con id (instance) 'users/:id/:id2/:id3'
app.get (URL_BASE+'users/:id/:id2',
    function(request,response){
//variable local let
      let indice = request.params.id;
      let indice2 = request.params.id2;
      console.log(indice);
      console.log(indice2);
      response.status(200);
      response.send(userFile[indice-1]);
      response.send(userFile[indice2-2]);
    });

//PETICION GET CON QUERY STRING

app.get(URL_BASE+'usersq',
    function(request,response){
      console.log(request.query);
      let query = request.query
      var datos = []
      console.log("maximo:"+query.max);
      for (var i = 0; i < query.max; i++) {
        datos.push(query.max-i);
      }
      console.log(datos);
      response.send(200);
    });

  //ENVIAR UN ID que no existe y controlar para que devuelva
  app.get(URL_BASE+'users/:id',
      function(request,response){
  //variable local let
        let indice = request.params.id;
        let respuesta =
            (userFile[indice-1] == undefined) ? {"msg": "Usuario No existe"}:userFile[indice-1]
        console.log(respuesta);
        response.status(200).send(respuesta);

      });

//OPERACIONES POST
app.post(URL_BASE+'users',
      function(request,response){
        console.log(request.body);
        console.log(request.body.id);
        //creo un objeto de javascript
        let newUser = {
              id: request.body.id,
              first_name:request.body.first_name,
              last_name: request.body.last_name,
              email: request.body.email,
              password: request.body.password
          }
          userFile.push(newUser);
          response.status(201);
          response.send({"msg": "Usuario creado correctamente",
                        "usuario": newUser});
          console.log(userFile);
    });


    //OPERACIONES POST sin mandar el ID
    app.post(URL_BASE+'users2',
          function(request,response){
            console.log(request.body);
            let cantUser=userFile.length
            //creo un objeto de javascript
            let newUser = {
                  id: cantUser+1,
                  first_name:request.body.first_name,
                  last_name: request.body.last_name,
                  email: request.body.email,
                  password: request.body.password
              }
              console.log(newUser);
              userFile.push(newUser);
              response.status(201);
              response.send({"msg": "Usuario creado correctamente",
                            "usuarios": userFile});

          });

// PUT PARA EDITAR
app.put(URL_BASE+'users2/:id',
      function(request,response){
        console.log(request.body);
        let indice = request.params.id;
        userFile[indice-1]
        console.log(userFile[indice-1].last_name) ;
        userFile[indice-1].last_name=request.body.last_name;
        console.log(userFile[indice-1]);
        response.status(201);
        response.send({"msg": "Usuario modificado",
                      "usuarios": userFile});

      });
//OPERACION DELETE
app.delete(URL_BASE+'users2/:id',
    function(request,response){
      let indice = request.params.id;
        //para eliminar
      let respuesta =
          (userFile[indice-1] == undefined) ? {"msg": "Usuario No Existe"}:
          {"Usuario eliminado": (userFile.splice(indice-1,1))};
      response.status(200).send(respuesta)
      console.log(userFile);
      //response.status(200).send("usuario eliminado"+userFile[indice-1]);

  });

//servidor escuchara en la url (servidor local)
//con control c para parar el servidor

app.listen(port,function() {
  console.log('NODE js ESCUCHANDO EN EL PUERTO 3000')
}); //funcion anonima
